#! /usr/bin/env/ python3

import os
import time
import logging

from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException

logging.basicConfig(
	level=logging.INFO,
	filename='logs',
	filemode='a',
	format='%(asctime)s %(name)s - %(levelname)s - %(message)s'
)


class StravaAuto:

	def __init__(self):
		self.email = os.environ.get('strava_email', None)
		self.password = os.environ.get('strava_pass', None)
		self.base_url ='https://www.strava.com'
		self.kudos_given = 0
		self.kudoed = 0
		self.non_activity = 0

		# Import optons for enable or disable browser option, here we are
		# disabling image loading with ChromeOptions for reducing loading time
		chrome_options = webdriver.ChromeOptions()
		# 1 - Allow all images
		# 2 - Block all images
		# 3 - Block 3rd party images 
		prefs = {"profile.managed_default_content_settings.images": 2}
		chrome_options.add_argument('--no-sandbox')
		chrome_options.add_argument('--window-size=1420,1080')
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('--disable-gpu')
		chrome_options.add_experimental_option("prefs", prefs)
		self.driver = webdriver.Chrome(options=chrome_options)

	def authorize(self):
		if not (self.email and self.password):
			exit("ERROR: Email and Password not configured!")

		# login with credentials
		self.driver.find_element_by_id('email').send_keys(self.email)
		self.driver.find_element_by_id ('password').send_keys(self.password)
		self.driver.find_element_by_id('login-button').click()

	def loadFeed(self):
	# Get all feed entries

		# load 30 activities
		self.driver.get(f'{self.base_url}/dashboard/following/30')

		if self.driver.current_url == f'{self.base_url}/login':
			self.authorize()

		feeds = self.driver.find_elements_by_class_name('feed-entry')

		for feed in feeds:

			try:
				# get kudo button
				kudo_btn = feed.find_element_by_xpath('./div/div/div[2]/button')
				kudo_btn_class_attribute = kudo_btn.get_attribute("class")

				# proceed only if activity is kudoable, there may be non kudoable entries in the feeds
				# for example if someone who you follow joined in a challenge that will be appear
				# the feeds which is not a kudoable feed-entry.
				if 'js-add-kudo' in kudo_btn_class_attribute:
					self.driver.execute_script("arguments[0].scrollIntoView();", kudo_btn)
					self.driver.execute_script("arguments[0].click();", kudo_btn)
					self.kudos_given += 1

				elif 'js-view-kudos' in kudo_btn_class_attribute:
					# already kudoed
					self.kudoed += 1

			except NoSuchElementException:
				# There are no child 'js-add-kudo' button
				self.non_activity += 1

		# log activity
		logging.info(f"FEEDS LOADED: {len(feeds)}")
		logging.info(f"KUDOS GIVEN: {self.kudos_given}")
		logging.info(f"ALREADY KUDOED: {self.kudoed}")
		logging.info(f"NON ACTIVITIES: {self.non_activity}")

		# quit driver, close browser instance
		self.driver.quit()


if __name__ == '__main__':

	 strava = StravaAuto()
	 strava.loadFeed()